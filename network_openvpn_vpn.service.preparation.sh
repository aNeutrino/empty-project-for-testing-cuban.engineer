#! /usr/bin/env bash
function error() {
  echo "$*" > /dev/stderr
  read -p "press enter to continue... "
  exit 1
}

function info() {
   echo "$*" > /dev/stderr
}

function exit_if_empty() {
  [[ x"$1"x == 'xx' ]] && error "empty variable"
}
function find_ovpn_file() {
  local VPN_URL="vpn00.aleph.engineering"
  local OVPN_CANDIDATES=$(find ~ | grep '\.ovpn$' || error "coud not find ovpn candidate files, please generate your VPN credentaials and download them")
  exit_if_empty ${OVPN_CANDIDATES}
  info "I have found those files as candidates configuration vpn files:"
  info "  ${OVPN_CANDIDATES}"
  local result=$(grep -l  ${VPN_URL} ${OVPN_CANDIDATES} | head -n 1 || error "coud not find ovpn files with ${VPN_URL} inside, check your email?")
  info "selecting:"
  info "  $result"
  echo ${result}
}

function comment_gateway_def() {
  cat $FILE_OVPN  \
  | sed -r 's/(^redirect-gateway def1)/# commented by blog.aleph.enginering script\n# \1/' \
  | sudo tee ${FILE_CONF} > /dev/null
}

function add_vpn_up_and_down_script_hooks() {
sudo tee -a ${FILE_CONF} > /dev/null << ENDOFMESSAGE
script-security 2
up ${VPN00_UP_SCRIPT}
down /etc/openvpn/update-resolv-conf
ENDOFMESSAGE

}

function create_vpn_up_script() {
sudo tee ${VPN00_UP_SCRIPT} > /dev/null << \ENDOFMESSAGE
#!/usr/bin/env sh

/etc/openvpn/update-resolv-conf $@

VPN_SERVER=51.75.170.29
VPN_NETWORK=10.222.0.0/16
VPN_IP=${4}
DEFAULT_ROUTER=$(ip r | grep default | grep -E '([0-9]+\.){3}[0-9]+' -o -m 1)
ROUTE_TO_VPN_EXISTS=$(ip r show ${VPN_SERVER} | wc -l)

ip r a ${VPN_NETWORK} via ${VPN_IP}
ip r a default via ${VPN_IP} metric 40
if [ ${ROUTE_TO_VPN_EXISTS} -eq 0 ]; then
        ip r a ${VPN_SERVER} via ${DEFAULT_ROUTER}
fi

ENDOFMESSAGE
sudo chmod +x ${VPN00_UP_SCRIPT}
}

FILE_OVPN=$(find_ovpn_file) || error "could not find ovpn file in your home directory."
FILE_CONF=/etc/openvpn/$(basename ${FILE_OVPN} | sed -r 's/(.*)\.ovpn/\1.conf/')
VPN_SERVICE_NAME=$(basename ${FILE_OVPN} | sed -r 's/(.*)\.ovpn/\1/')
VPN00_UP_SCRIPT=/etc/openvpn/vpn00.up.sh

sudo apt install openvpn
comment_gateway_def
add_vpn_up_and_down_script_hooks
create_vpn_up_script

sudo service openvpn@${VPN_SERVICE_NAME} restart
